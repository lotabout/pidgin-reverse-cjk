Reverse CJK

## Description
This is a feature just for fun.

When sending messages, this plugin will reverse the order of
CJK(Chinese/Japanese/Korean) characters. Note it will no work with ascii
characters. 

## Example
When sending message "English 中文 中 end", it will turn this message into
"English 中 文中 end" and then send it.

## Contacts:
For bugs reports, hints, … email me at lotabout( at ) gmail.com.

## How to install:
Read the INSTALL file.

## How to use:
1.  Compile and Install (read INSTALL).

2.  Linux: if you don't have GNU make program installed on your system, to 
    install the plugin manually copy jabber-pseudo-invisibility.so in your 
    purple home dir:
    
    (if ~/.purple/plugins/ does not exist: $ mkdir ~/.purple/plugins/ )
    $ cp reverse-cjk.so ~/.purple/plugins/

    Windows: Move reverse-cjk.dll in Pidgin plugins dir (I 
    suppose you can create 
    C:\Documents and Settings\<user>\Applications data\.purple\plugins 
    and move the plugin there). In Windows it should be something like 
    C:\Programmi\Pidgin\plugins\

3.  In Pidgin->Tools->Plugins, tick the "Reverse CJK" plugin. Then All CJK 
    characters will be reversed.
