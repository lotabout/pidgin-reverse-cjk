/*
 * Displays messages on a new line, below the nick
 * Copyright (C) 2004 Stu Tomlinson <stu@nosnilmot.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02111-1301, USA.
 */

#define PLUGIN_ID           "reversecjk"
#define PLUGIN_NAME         "Reverse CJK"
#define PLUGIN_VERSION      "0.1.0"
#define PLUGIN_STATIC_NAME	"reverse-cjk"
#define PLUGIN_SUMMARY		"Reverse CJK characters when sending messages"
#define PLUGIN_DESCRIPTION	"Reverse CJK characters when sending messages"
#define PLUGIN_AUTHOR		"Mark Wallace <lotabout@gmail.com>"
#define PLUGIN_WEBSITE		"NONE"
#define PREF_PREFIX         "/plugins/core/"PLUGIN_ID


#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

/* config.h may define PURPLE_PLUGINS; protect the definition here so that we
 * don't get complaints about redefinition when it's not necessary. */
#ifndef PURPLE_PLUGINS
# define PURPLE_PLUGINS
#endif

#ifdef GLIB_H
# include <glib.h>
#endif

/* This will prevent compiler errors in some instances and is better explained in the
 * how-to documents on the wiki */
#ifndef G_GNUC_NULL_TERMINATED
# if __GNUC__ >= 4
#  define G_GNUC_NULL_TERMINATED __attribute__((__sentinel__))
# else
#  define G_GNUC_NULL_TERMINATED
# endif
#endif

#include <string.h>

#include <conversation.h>
#include <debug.h>
#include <plugin.h>
#include <signals.h>
#include <util.h>
#include <version.h>


/* [中国] => [国中] */
void reverse(gunichar *start, gunichar * end)
{
    gunichar *f=start;
    gunichar *l=end;
    gunichar tmp;
    while (f<l)
    {
        tmp = *l;
        *l = *f;
        *f = tmp;
        f++;
        l--;
    }
}

/* [eng中国eng] => [eng国中eng] */
gchar * reverse_chinese(const gchar * str)
{
    glong items_written;
    gunichar * wstr = g_utf8_to_ucs4_fast(str, -1, &items_written);

    gunichar *cjk_start= wstr;
    gunichar *cjk_end = wstr;
    gunichar *p = wstr;
    gunichar *end = wstr + items_written - 1;
    int is_cjk=0;

    while(p<=end)
    {
        if (! g_unichar_iswide_cjk(*p) && is_cjk)
        {
            cjk_end = p-1;
            reverse(cjk_start, cjk_end);
            is_cjk = 0;
        }
        else if (! is_cjk)
        {
            cjk_start = p;
            is_cjk = 1;
        }
        p++;
    }
    if (is_cjk)
    {
        cjk_end = p-1;
        reverse(cjk_start, cjk_end);
        is_cjk = 0;
    }

    gchar * utf8str;
    utf8str = g_ucs4_to_utf8(wstr, items_written, NULL, NULL, NULL);
    free(wstr);
    return utf8str;
}

static gboolean
reverse_msg_sm(PurpleAccount *account, char *receiver, char **message)
{
    gchar * tmp = reverse_chinese(*message);
    g_free(*message);
    *message = tmp;
    return FALSE;
}

static gboolean
reverse_msg_cb(PurpleAccount *account, char *sender, char **message,
					 PurpleConversation *conv, int flags)
{
	if (((purple_conversation_get_type(conv) == PURPLE_CONV_TYPE_IM) &&
		 !purple_prefs_get_bool(PREF_PREFIX"/im")) ||
		((purple_conversation_get_type(conv) == PURPLE_CONV_TYPE_CHAT) &&
		 !purple_prefs_get_bool(PREF_PREFIX"/chat")))
		return FALSE;


    if (flags & PURPLE_MESSAGE_SEND)
    {
        gchar * tmp = reverse_chinese(*message);
        g_free(*message);
        *message = tmp;
        /* print debug message*/
        /*purple_debug_info(PLUGIN_ID, "AFTER: %s\n", tmp);*/
    }

	return FALSE;
}

static PurplePluginPrefFrame *
get_plugin_pref_frame(PurplePlugin *plugin) {
	PurplePluginPrefFrame *frame;
	PurplePluginPref *ppref;

	frame = purple_plugin_pref_frame_new();

	ppref = purple_plugin_pref_new_with_name_and_label(
			PREF_PREFIX"/im", "Show Reverse output in IMs");
	purple_plugin_pref_frame_add(frame, ppref);

	ppref = purple_plugin_pref_new_with_name_and_label(
			PREF_PREFIX"/chat", "Show Reversed output in Chats");
	purple_plugin_pref_frame_add(frame, ppref);

	return frame;
}


static gboolean
plugin_load(PurplePlugin *plugin)
{
	void *conversation = purple_conversations_get_handle();

    purple_signal_connect(conversation, "sending-im-msg",
                        plugin, PURPLE_CALLBACK(reverse_msg_sm), NULL);
    purple_signal_connect(conversation, "sending-chat-msg",
                        plugin, PURPLE_CALLBACK(reverse_msg_sm), NULL);
    purple_signal_connect(conversation, "writing-im-msg",
                        plugin, PURPLE_CALLBACK(reverse_msg_cb), NULL);
    purple_signal_connect(conversation, "writing-chat-msg",
                        plugin, PURPLE_CALLBACK(reverse_msg_cb), NULL);
	return TRUE;
}

static PurplePluginUiInfo prefs_info = {
	get_plugin_pref_frame,
	0,   /* page_num (Reserved) */
	NULL, /* frame (Reserved) */
	/* Padding */
	NULL,
	NULL,
	NULL,
	NULL
};

static PurplePluginInfo info =
{
	PURPLE_PLUGIN_MAGIC,							/**< magic			*/
	PURPLE_MAJOR_VERSION,							/**< major version	*/
	PURPLE_MINOR_VERSION,							/**< minor version	*/
	PURPLE_PLUGIN_STANDARD,							/**< type			*/
	NULL,											/**< ui_requirement	*/
	0,												/**< flags			*/
	NULL,											/**< dependencies	*/
	PURPLE_PRIORITY_DEFAULT,						/**< priority		*/
    PLUGIN_ID,
	PLUGIN_NAME,
	PLUGIN_VERSION,
	PLUGIN_SUMMARY,
	PLUGIN_DESCRIPTION,
	PLUGIN_AUTHOR,
	PLUGIN_WEBSITE,
	plugin_load,
	NULL,
	NULL,
	NULL,
	NULL,
	&prefs_info,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL
};

static void
init_plugin(PurplePlugin *plugin) {
	purple_prefs_add_none(PREF_PREFIX);
	purple_prefs_add_bool(PREF_PREFIX"/im", TRUE);
	purple_prefs_add_bool(PREF_PREFIX"/chat", TRUE);
}

PURPLE_INIT_PLUGIN (PLUGIN_ID, init_plugin, info)
